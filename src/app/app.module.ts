import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule, ReactiveFormsModule , Validators} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FrontendModule } from './frontend/frontEnd.module';
import { RegisterComponent } from './frontend/pages/register/register.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { DashboardModule } from './dashboard/dashboard.module';
import { NgxLoadingModule } from 'ngx-loading';
import { AuthGuard } from './guards/auth.guard';

@NgModule({
  declarations: [
    AppComponent,
  
  ],
  imports: [
    BrowserModule,
    FrontendModule,
    RouterModule,
    DashboardModule,
    NgxLoadingModule.forRoot({}),
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy},AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
