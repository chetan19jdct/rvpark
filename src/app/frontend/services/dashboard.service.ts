import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private base_url = environment.engineUrl;
   constructor(private http: HttpClient) {
    }
   headerCommon() {
    return {
      headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
    }; 
  }
  addUser(data):Observable<any> {
    let params = new HttpParams({fromObject:data});  
     return this.http.post(this.base_url+'user/addUser', params, this.headerCommon()).pipe(map((response: Response) => {
        return response;
      }
    ));
  } 
  updateUser(data):Observable<any> {
    let params = new HttpParams({fromObject:data});  
    return this.http.post(this.base_url+'user/updateUser', params, this.headerCommon()).pipe(map((response: Response) => {
       return response;
     }
   ));
  }
  checkLogin(data):Observable<any> {
    let params = new HttpParams({fromObject:data});  
     return this.http.post(this.base_url+'user/checkLogin', params, this.headerCommon()).pipe(map((response: Response) => {
        return response;
      }
    ));
  }
}
