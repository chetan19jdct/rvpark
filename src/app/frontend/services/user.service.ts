import { Injectable, EventEmitter, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { map, filter, switchMap } from 'rxjs/operators';
import { HttpParams ,HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  @Output() onLanguageChange: EventEmitter<any> = new EventEmitter();
  private base_url = environment.engineUrl;
  constructor(private http: HttpClient) { }
  CurrrentLoggedinUser(){
    return JSON.parse(localStorage.getItem('users'));
  }
  headerCommon() {
    return {
      headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
    }; 
  }
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      console.log(error);
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
  loggedOutUser(){
    localStorage.removeItem('users');
    localStorage.removeItem('cart');
    this.onLanguageChange.emit();
  }
  contactSubmit(data):Observable<any> {
    let params = new HttpParams({fromObject:data});  
     return this.http.post(this.base_url+'user/sendContact', params, this.headerCommon()).pipe(map((response: Response) => {
        return response;
      }
    ));
  }
  getUserDetail(userID):Observable<any> {
   let params = new HttpParams().set('userID',userID);
  /* return this.http.post(this.base_url+'user/userDetail', params, this.headerCommon()).pipe(map((response: Response) => {
      return response;
    }
   ));
   */
    return this.http.post(this.base_url+'/user/userDetail', params,this.headerCommon());
  
  }
  forgetPass(datas):Observable<any> {
      let params = new HttpParams({fromObject:datas}); 
      return this.http.post(this.base_url+'/user/forgetemail', params,this.headerCommon()).pipe(map((response: Response) => {
         return response;
      }
    )); 
  } 
checkEncryptKey (emailKey) :Observable<any> { 
      let params = new HttpParams().set('encryKey',emailKey); 
      return this.http.post(this.base_url+'/user/encryKeyCheck', params,this.headerCommon()).pipe(map((response: Response) => {
        return response;
      }
    )); 
  } 
changePass(data) :Observable<any> { 
    let params = new HttpParams({fromObject:data});  
      return this.http.post(this.base_url+'/user/changePass', params,this.headerCommon()).pipe(map((response: Response) => {
       return response;
      }
    )); 
  }  
}
