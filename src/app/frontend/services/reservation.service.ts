import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';
import { map, filter, switchMap } from 'rxjs/operators';
import { UserService } from './user.service';
import { environment } from '../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  private base_url = environment.engineUrl;
  constructor(private http: HttpClient,private currentUser:UserService) {}
  headerCommon() {
    return {
      headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
    }; 
  }
  /** Reservation  */
  reservatoin(reservationFormValues,transId,status,ammount,paymentMode):Observable<any> { 
    var checkLoggedInUser = this.currentUser.CurrrentLoggedinUser();
    var currenUserId = checkLoggedInUser[0].id;
    let params = new HttpParams({fromObject:reservationFormValues}).set('userId',currenUserId)
                                                                    .set('transitionId',transId)
                                                                    .set('status',status)
                                                                    .set('paymentmode',paymentMode)
                                                                    .set('ammount',ammount);
      return this.http.post(this.base_url+'user/reservation', params, this.headerCommon()).pipe(map((response: Response) => {
        return response;
      } 
    ));
  } 
  reservatoinCart(reservationFormValues) {
    // var checkLoggedInUser = this.currentUser.CurrrentLoggedinUser();
    // var currenUserId = checkLoggedInUser[0].id;
    localStorage.setItem('cart',  JSON.stringify(reservationFormValues));
  } 
  /** Reservation  */
  findAlreadyReser(datesBooked):Observable<any> {
    let params = new HttpParams({fromObject:datesBooked});
      return this.http.post(this.base_url+'user/findReserved', params, this.headerCommon()).pipe(map((response: Response) => {
        return response;
      }
    ));
  }
  /* Already Reserved */
  reservedPrice(siteData,days,discountpercentage,tentSite):Observable<any> {
    let params = new HttpParams().set('reservationSite',siteData)
                                   .set('booking_days',days)
                                   .set('discount_percentage',discountpercentage)
                                   .set('reservationSiteTent',tentSite);
    return this.http.post(this.base_url+'user/reservationData', params, this.headerCommon()).pipe(map((response: Response) => {
      return response;
    }
  ));
  } 
  /* Already Reserved end */
  userOrder(userID):Observable<any> {
    let params = new HttpParams().set('user_id',userID);
    return this.http.post(this.base_url+'user/userReservation', params, this.headerCommon()).pipe(map((response: Response) => {
      return response;
    }
    ));
  }
  loadColor() {
    return this.http.get(this.base_url+'user/loadColor', this.headerCommon()).pipe(map((response: Response) => {
      return response;
    }
  ));
    
  }
} 
