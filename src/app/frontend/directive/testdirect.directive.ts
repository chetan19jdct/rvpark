import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[appABCD]'
})
export class TestdirectDirective {
  @Input() appABCD: string;
  constructor() {}
  ngOnInit() {
    alert(this.appABCD);
  }
}
