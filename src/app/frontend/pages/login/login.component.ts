import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DashboardService } from '../../services/dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loading = false;
  testdirects = 25
  submitted;
  cartItem;
  emailaddress = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'; 
  constructor( private dashboardservice: DashboardService,
    private activeRoute: ActivatedRoute,
    private router: Router) {}
ngOnInit() {
  this.cartItem = JSON.parse(localStorage.getItem('cart'));
  console.log(this.cartItem);
}
  loginForm = new FormGroup({
    loginemail : new FormControl('', [Validators.email]),
    password : new FormControl(),
  })
  get f() { return this.loginForm.controls; }
  errorRedirect(ew) {
if(ew == "another_email") {
      this.router.navigateByUrl('/LoginComponent', {skipLocationChange: true}).then(()=>
      this.router.navigate(["/login"])); 
  } else {
    
    this.router.navigate(["/"]); 
  }
}
  loginSubmit(loginForm) {
    this.submitted = true;
    var CondiDivStyle =  document.getElementsByClassName("CondiDiv") as HTMLCollectionOf<HTMLElement>;
    CondiDivStyle[0].style.display = "none"; 
    var loginFormValue = this.loginForm.value;
    if (!this.loginForm.invalid) {
        this.loading = true; 
        this.dashboardservice.checkLogin(loginFormValue).subscribe((res) => {
            this.loading = false;
            if((res.resp)) {
              var sucssMsg =  document.getElementsByClassName("success_msg") as HTMLCollectionOf<HTMLElement>;
              sucssMsg[0].innerHTML = "User logged in successfully";
              localStorage.setItem('users', JSON.stringify(res.data));
              if(this.cartItem === null) {
                 this.router.navigate(['/profile']);
              } else {
                this.router.navigateByUrl('/BookingCartComponent', {skipLocationChange: true}).then(()=>
                this.router.navigate(["/bookingcart"])); 
              }
            } else {
              alert("Invalid Credential" );
              CondiDivStyle[0].style.display = "block"; 
            }
        });
    } 
  }
}