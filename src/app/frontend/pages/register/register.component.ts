import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import {  FormGroup, FormControl, Validators } from '@angular/forms';
import {DashboardService} from "../../services/dashboard.service";
import {Router,ActivatedRoute} from '@angular/router';
import { RouterModule } from '@angular/router';
import { AngularWaitBarrier } from 'blocking-proxy/built/lib/angular_wait_barrier';
import { ValueTransformer } from '@angular/compiler/src/util';

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  states = {'Alabama': 'AL', 'Alaska': 'AK','Arizona': 'AZ',
  'Arkansas': 'AR',
  'California': 'CA',
  'Colorado': 'CO',
  'Connecticut': 'CT',
  'Delaware': 'DE',
  'Florida': 'FL',
  'Georgia': 'GA',
  'Hawaii': 'HI',
  'Idaho': 'ID',
  'Illinois': 'IL',
  'Indiana': 'IN',
  'Iowa': 'IA',
  'Kansas': 'KS',
  'Kentucky': 'KY',
  'Louisiana': 'LA',
  'Maine': 'ME',
  'Maryland': 'MD',
  'Massachusetts': 'MA',
  'Michigan': 'MI',
  'Minnesota': 'MN',
  'Mississippi': 'MS',
  'Missouri': 'MO',
  'Montana': 'MT',
  'Nebraska': 'NE',
  'Nevada': 'NV',
  'New Hampshire': 'NH',
  'New Jersey': 'NJ',
  'New Mexico': 'NM',
  'New York': 'NY',
  'North Carolina': 'NC',
  'North Dakota': 'ND',
  'Ohio': 'OH',
  'Oklahoma': 'OK',
  'Oregon': 'OR',
  'Pennsylvania': 'PA',
  'Rhode Island': 'RI',
  'South Carolina': 'SC',
  'South Dakota': 'SD',
  'Tennessee': 'TN',
  'Texas': 'TX',
  'Utah': 'UT',
  'Vermont': 'VT',
  'Virginia': 'VA',
  'Washington': 'WA',
  'West Virginia': 'WV',
  'Wisconsin': 'WI',
  'Wyoming': 'WY'}; 
  submitted = false;
  public loading = false;
  onlyCharcter = "^[a-zA-Z ]+$";
  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$"; 
  zipcode = "^((\\+91-?)|0)?[0-9]{5}$"; 
  emailaddress = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'; 
  password = "(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}"; //At least 8 characters in length , Lowercase letters, Uppercase letters, Numbers, Special characters
  intOnly = "^\d+$";
  stateempty = "";
  lice_stateempty = '';
  constructor(  private dashboardservice: DashboardService,
                private activeRoute: ActivatedRoute,
                private router: Router,
                private elRef:ElementRef
  ) {}
  
  ngOnInit() {
    $("#default-settings").birthdayPicker();	
    $("#default-birthday").birthdayPicker({"defaultDate":"01-01-1985"});
   }
   
  registrationForm = new FormGroup({
    fname : new FormControl('', [Validators.pattern(this.onlyCharcter)]),
    lname : new FormControl('', [Validators.pattern(this.onlyCharcter)]),
    saddress : new FormControl(),
    city : new FormControl('', [Validators.pattern(this.onlyCharcter)]), 
    state : new FormControl('', [Validators.pattern(this.onlyCharcter)]), 
    zipcode : new FormControl('', [Validators.pattern(this.zipcode)]), 
    email : new FormControl('', [Validators.email]),
    phone : new FormControl('', [Validators.pattern(this.mobnumPattern)]),
    email1 : new FormControl(),
    phone2 : new FormControl(),
    password : new FormControl('', [Validators.pattern(this.password)]),
    confirm_password : new FormControl('',  [Validators.required]),
    defaultbirthday_birthDay : new FormControl(),
    drivingLic : new FormControl(),
    licen_state : new FormControl('', [Validators.pattern(this.onlyCharcter)]),
    d_l_number : new FormControl(),
    adults : new FormControl(),
    kids : new FormControl(),
    pets : new FormControl(),
    total_member : new FormControl(),
    pickup_auto : new FormControl(),
    motor_home :   new FormControl(),
    pickup_trailer : new FormControl(),
    pickup_fifthe_wheel : new FormControl(),
    pickup_boat : new FormControl(),
    pickup_other : new FormControl(),
    licence_plate_number : new FormControl(),
    long_vehicle_length : new FormControl(),

  },{validators: this.passwordMatchValidator})
  passwordMatchValidator(registrationForm) {
   var testqwe = registrationForm.controls.password.value === registrationForm.controls.confirm_password.value ? null : {'mismatch': true}; 
  
   return testqwe;
  }
 get f() { 
    return this.registrationForm.controls; 
  }
  goToNextSlide(event){
   if(this.registrationForm.invalid) {
      this.submitted = true;
    } else {
      $('.mainsectionC').removeClass('pactive').next('.mainsectionC').addClass('pactive');
    } 
  }
  onlypositiveNumbers(event){ 
    if(event.keyCode == 189 || event.keyCode == 109) {
         event.preventDefault();
    }
  }
  audioSubmit(registrationForm) {
    
    this.registrationForm.value.defaultbirthday_birthDay = $(".birthDay").val();
    this.submitted = true;
    let controls  = this.registrationForm.controls;
    
    for (const name in controls) {
      if (controls[name].invalid) {
         console.log(name);
      }
    }
    if (this.registrationForm.invalid) {
       let previousSlide =  document.getElementsByClassName("prevslides") as HTMLCollectionOf<HTMLElement>;
       previousSlide[0].click(); 
     } else {
      this.loading = true;
      const register_user_date = this.registrationForm.value;
      this.dashboardservice.addUser(register_user_date).subscribe((res) => {
        this.loading = false;
        if(res.resp){
            alert("user Addedd successfully");
            window.location.reload();
          } else {
            alert(res.Message); 
            let previousSlide =  document.getElementsByClassName("prevslides") as HTMLCollectionOf<HTMLElement>;
             previousSlide[0].click(); 
         }
      })
    }
  }
  memberChange(chnageVal) {
    let petss =  this.elRef.nativeElement.querySelector('#kids').value;
    let sumvalues =  this.elRef.nativeElement.querySelector('.adults').value ;
    let pickup_auto =  this.elRef.nativeElement.querySelector('.pets').value ;
    var  totalMemberVal =  (parseInt(petss)+parseInt(sumvalues)+parseInt(pickup_auto));
    this.registrationForm.controls['total_member'].setValue(totalMemberVal);
  }
  onlyzipcode(cts) {
    var reg = "^((\\+91-?)|0)?[0-9]{5}$";
    if(cts.match(reg) !== null) {
      return false;
    }  
  }
  onlynumber(evt) {
    evt.preventDefault();
  }
}
