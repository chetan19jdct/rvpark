import { Component, OnInit } from '@angular/core';
import { ReservationService } from '../../services/reservation.service';
import { UserService } from '../../services/user.service';
declare var $: any;

@Component({
  selector: 'app-user-order',
  templateUrl: './user-order.component.html',
  styleUrls: ['./user-order.component.css']
})
export class UserOrderComponent implements OnInit {
  public loading = true;
  constructor(private reservationService: ReservationService,private userService:UserService) {}
  reservationList;
  ngOnInit() {
     $('#exampleEwss').DataTable();
     var userDetail = this.userService.CurrrentLoggedinUser();
    var userrId = userDetail[0].id;
    this.reservationService.userOrder(userrId).subscribe((res) => {
      this.loading = false;
          this.reservationList = res.data;
        
      }) 
  } 
}
