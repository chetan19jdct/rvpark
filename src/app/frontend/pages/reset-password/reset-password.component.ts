import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  constructor(private activeRoute: ActivatedRoute,private userService: UserService) { }
  isvalidKey = false;
  submitted; 
  userID;
  password = "(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}"; //At least 8 characters in length , Lowercase letters, Uppercase letters, Numbers, Special characters
  changePass = new FormGroup({
      first_password : new FormControl('', [Validators.pattern(this.password)]),
      confirm_pass : new FormControl('', [Validators.pattern(this.password)]),
      user_id: new FormControl('')
   })
  ngOnInit() {
    const routeParams = this.activeRoute.snapshot.params;
    if(routeParams.key != "") {
      this.userService.checkEncryptKey(routeParams.key).subscribe((res)=> {
        if(res.Message == 'Data Found') {
          this.isvalidKey  = true;
          this.userID  = res.data[0].id;
        } else {
          this.isvalidKey  = false;

        } 
      })
    }
  }

  changePassFun(changePass) {
   
    this.submitted = true;
    if(this.changePass.valid) {
      console.log(this.changePass.value);
       if(this.changePass.value.first_password ===  this.changePass.value.confirm_pass) {
          this.userService.changePass(this.changePass.value).subscribe((res)=> {
             alert('Password Updated Successfully');
             
          })
       } else {
          alert('Both password is not matched');
       }
    }
  }
}
