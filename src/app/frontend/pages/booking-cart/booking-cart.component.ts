import { Component, OnInit, ElementRef } from '@angular/core';
import { ReservationService } from '../../services/reservation.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import {IPayPalConfig, ICreateOrderRequest} from 'ngx-paypal';
declare var $: any;
@Component({
  selector: 'app-booking-cart',
  templateUrl: './booking-cart.component.html',
  styleUrls: ['./booking-cart.component.css']
})
export class BookingCartComponent implements OnInit {
  bookingCart;
  startdate;
  endDate;
  currency;
  currencyPaypal;
  discounttotalss;
  public loading = false;
  public cartItemsResponse; 
  
  constructor(private reservationService: ReservationService,
              private activeRoute: ActivatedRoute,
              private router: Router,
              private elRef:ElementRef) {}
              public payPalConfig?: IPayPalConfig;
              showSuccess;
              
  ngOnInit(): void {
   
    this.currency = environment.currency;
    this.currencyPaypal = environment.currencyPaypal;
    this.loading = true;
    this.bookingCart = JSON.parse(localStorage.getItem('cart'));
    
    var startdate = this.bookingCart.startdate;
    var endate = this.bookingCart.endate;
 
    this.startdate = startdate;
    this.endDate = endate;
    /* ************************** No of days ************************************* */
    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
    var firstDate = new Date(startdate);
    var secondDate = new Date(endate);
    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
        diffDays = diffDays+1;
    /* ************************** No of days end *********************************** */
    /* booking sites */   
    this.bookingCart.sitesNewArray;  
    this.bookingCart.sitesNewArrayTent;

    
    var alpha1 = [];
    var alpha2 = [];
    // var totalSites = alpha1.concat(JSON.parse(this.bookingCart.extra_pullthru),JSON.parse(this.bookingCart.tents),JSON.parse(this.bookingCart.semi_pullthru),JSON.parse(this.bookingCart.lakefront),JSON.parse(this.bookingCart.rentalunit),JSON.parse(this.bookingCart.backin),JSON.parse(this.bookingCart.standard_pullthru),); 
    var totalSites = alpha1.concat(JSON.parse(this.bookingCart.sitesNewArray),); 
    var totalSitesTent = this.bookingCart.sitesNewArrayTent; 
  
    /* ************************** Booking Sites Service  ************************************* */
    this.loading = true; 
   
    this.reservationService.reservedPrice(totalSites,diffDays,this.bookingCart.percentage,totalSitesTent).subscribe((res) => {
      this.loading = false;  
      if(res.resp) { 
        this.cartItemsResponse = res.data; 
        this.initConfig(this.cartItemsResponse);
      } else {
        alert('Something went wrong');
      }
    })
    /* ************************** Booking Sites Service end ********************************** */

    
    /* booking sites end */
  }
  /* Paypal */
  private initConfig(re): void {
    
      var  allSites = re.sites;
      var allSitesItem = [];
      allSites.forEach((allSitesss) => { 
       if(allSitesss.booking_type == 'per_month') {
          var siteNo = allSitesss.site_no; 
         // var totalPrice = allSitesss.day_calculation + allSitesss.per_month_price; 
          var totalPrice =  allSitesss.total_price;
          allSitesItem.push({
                name: 'Site No:'+siteNo,
                quantity: '1',
                category: 'DIGITAL_GOODS',
                unit_amount: {
                  currency_code: this.currencyPaypal, 
                  value: totalPrice,  
                }
          });
       } else {
          var siteNo = allSitesss.site_no; 
          var totalPrice = allSitesss.total_price; 
          allSitesItem.push({
                name: 'Site No:'+siteNo,
                quantity: '1',
                category: 'DIGITAL_GOODS',
                unit_amount: {
                  currency_code: this.currencyPaypal, 
                  value: totalPrice,  
                }
          });
       } 
    })
    var discountTotal = re.total - re.new_total;
    this.discounttotalss = Math.round(discountTotal * 100) / 100;
    this.payPalConfig = {
    currency: this.currencyPaypal,
    clientId: 'Ad9u2rPyzDhjsmQyLK55Yx8icTnKetv24_GdlJX6GGa8Tv2YMScNG3NhQF9HYNw6EK8roQhkhISdd5Hk',
    createOrderOnClient: (data) => <ICreateOrderRequest>{
      intent: 'CAPTURE',
      purchase_units: [
        {
          "amount": {
              "currency_code": this.currencyPaypal,
              "value": re.new_total,
              "breakdown": {
                  "item_total": {
                      "currency_code": this.currencyPaypal,
                      "value": re.total
                  },
                  "discount": {
                      "currency_code": this.currencyPaypal,
                      "value": this.discounttotalss
                  }
              }
          },
          "items": allSitesItem
      }
      ]
    },
    advanced: {
      commit: 'true'
    }, 
    style: {
      label: 'checkout',
      layout: 'vertical',
      color:'gold',
    },
    onApprove: (data, actions) => {
      this.loading = true;
      console.log('11  onApprove - transaction was approved, but not authorized', data, actions);
      actions.order.get().then(details => {
        console.log('6777  onApprove - you can get full order details inside onApprove: ', details); 
      });
    },
    onClientAuthorization: (data) => {
      console.log('22 onClientAuthorization - you should probably inform your server about completed transaction at this point', data);
      this.showSuccess = true;
      console.log(data);
      this.orderPlaced(data);
    },
    onCancel: (data, actions) => {
      console.log('33 OnCancel', data, actions);
    },
    onError: err => {
      console.log('44 OnError', err);
    },
    onClick: () => {
      console.log('55 onClick');
    },
  };
  }
  /* paypal end */
  paymentPaypal(event) {
    let element: HTMLElement = document.getElementsByClassName('finalPaypal')[0] as HTMLElement;
    element.click();
  }
  orderPlaced(orderData) {
      var transId = orderData.id;
      var status = orderData.status;
      var ammount = orderData.purchase_units[0].amount.value;
      var orderData = orderData.purchase_units[0].items;
      this.reservationService.reservatoin(this.bookingCart,transId,status,ammount,'online').subscribe((res) => {
          this.loading = false;
          alert("Thank you for Reservation. Will Reply you Soon");
          window.location.href= '/#/reservation/';
      })
      /* */
  }
  paymentoption(er) {
      if(er.target.value == 'paypal') {
          $("#lst_paypal").css('display','block');
          $("#cash_pay").css('display','none');
          $("#cheque_pay").css('display','none');
      } else if(er.target.value == 'cash') {
          $("#lst_paypal").css('display','none');
          $("#cash_pay").css('display','block');
          $("#cheque_pay").css('display','none');
      } else {
          $("#lst_paypal").css('display','none');
          $("#cash_pay").css('display','none');
          $("#cheque_pay").css('display','block');
      }
  }
  submitPayment(even,paymentMode) {
    var transId = "";
    var status = "Pending";
    var ammount = this.cartItemsResponse.new_total;
    this.reservationService.reservatoin(this.bookingCart,transId,status,ammount,paymentMode).subscribe((res) => {
      this.loading = false;
      
      alert("Thank you for Reservation. Will Reply you Soon");
    window.location.href= '/#/reservation/';
    })
  }
  
}