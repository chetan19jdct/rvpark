import { Component, OnInit, ElementRef } from '@angular/core';
import { UserService } from '../../services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {DashboardService} from "../../services/dashboard.service";
import { ActivatedRoute, Router } from '@angular/router';
declare var $: any;
@Component({
  selector: 'app-userprofile',
  templateUrl: './userprofile.component.html',
  styleUrls: ['./userprofile.component.css']
})
export class UserprofileComponent implements OnInit {
  userDetail; 
  states = {'Alabama': 'AL', 'Alaska': 'AK','Arizona': 'AZ',
  'Arkansas': 'AR',
  'California': 'CA',
  'Colorado': 'CO',
  'Connecticut': 'CT',
  'Delaware': 'DE',
  'Florida': 'FL',
  'Georgia': 'GA',
  'Hawaii': 'HI',
  'Idaho': 'ID',
  'Illinois': 'IL',
  'Indiana': 'IN',
  'Iowa': 'IA',
  'Kansas': 'KS',
  'Kentucky': 'KY',
  'Louisiana': 'LA',
  'Maine': 'ME',
  'Maryland': 'MD',
  'Massachusetts': 'MA',
  'Michigan': 'MI',
  'Minnesota': 'MN',
  'Mississippi': 'MS',
  'Missouri': 'MO',
  'Montana': 'MT',
  'Nebraska': 'NE',
  'Nevada': 'NV',
  'New Hampshire': 'NH',
  'New Jersey': 'NJ',
  'New Mexico': 'NM',
  'New York': 'NY',
  'North Carolina': 'NC',
  'North Dakota': 'ND',
  'Ohio': 'OH',
  'Oklahoma': 'OK',
  'Oregon': 'OR',
  'Pennsylvania': 'PA',
  'Rhode Island': 'RI',
  'South Carolina': 'SC',
  'South Dakota': 'SD',
  'Tennessee': 'TN',
  'Texas': 'TX',
  'Utah': 'UT',
  'Vermont': 'VT',
  'Virginia': 'VA',
  'Washington': 'WA',
  'West Virginia': 'WV',
  'Wisconsin': 'WI',
  'Wyoming': 'WY'}; 
  constructor(private userService:UserService,
    private dashboardservice: DashboardService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    private elRef:ElementRef) { }
  submitted = false;
  public loading = false;
  onlyCharcter = "^[a-zA-Z ]+$";
  mobnumPattern = "^((\\+91-?)|0)?[0-9]{10}$"; 
  zipcode = "^((\\+91-?)|0)?[0-9]{5}$"; 
  emailaddress = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'; 
  password = "(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}"; //At least 8 characters in length , Lowercase letters, Uppercase letters, Numbers, Special characters
  intOnly = "^\d+$";
  stateempty = "";
  lice_stateempty = '';

  registrationForm = new FormGroup({
    fname : new FormControl('', [Validators.pattern(this.onlyCharcter)]),
    userId : new FormControl(), 
    lname : new FormControl('', [Validators.pattern(this.onlyCharcter)]),
    saddress : new FormControl(),
    city : new FormControl('', [Validators.pattern(this.onlyCharcter)]), 
    state : new FormControl('', [Validators.pattern(this.onlyCharcter)]), 
    zipcode : new FormControl('', [Validators.pattern(this.zipcode)]), 
    email : new FormControl('', [Validators.email]),
    phone : new FormControl('', [Validators.pattern(this.mobnumPattern)]),
    email1 : new FormControl(),
    phone2 : new FormControl(),
   
    defaultbirthday_birthDay : new FormControl(),
    drivingLic : new FormControl(),
    licen_state : new FormControl('', [Validators.pattern(this.onlyCharcter)]),
    d_l_number : new FormControl(),
    adults : new FormControl(),
    kids : new FormControl(),
    pets : new FormControl(),
    total_member : new FormControl(),
    pickup_auto : new FormControl(),
    motor_home : new FormControl(),
    pickup_trailer : new FormControl(),
    pickup_fifthe_wheel : new FormControl(),
    pickup_boat : new FormControl(),
    pickup_other : new FormControl(),
    licence_plate_number : new FormControl(),
    long_vehicle_length : new FormControl(),

  })
 
 get f() { 
    return this.registrationForm.controls; 
  }
  goToNextSlide(event){
    if(this.registrationForm.invalid){
      console.log(this.registrationForm);
      this.submitted = true;
    } else {
      $('.mainsectionC').removeClass('pactive').next('.mainsectionC').addClass('pactive');
    } 
  }
  onlypositiveNumbers(event){ 
    if(event.keyCode == 189 || event.keyCode == 109) {
         event.preventDefault();
    }
  }
  audioSubmit1(registrationForm) {
    this.registrationForm.value.defaultbirthday_birthDay = $(".birthDay").val();
    this.submitted = true;
    let controls  = this.registrationForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
         console.log(name);
      }
    }
    if (this.registrationForm.invalid) {
       let previousSlide =  document.getElementsByClassName("prevslidesss") as HTMLCollectionOf<HTMLElement>;
       previousSlide[0].click(); 
     } else {
      this.loading = true;
      const register_user_date = this.registrationForm.value;
      // console.log(register_user_date);
      
      this.dashboardservice.updateUser(register_user_date).subscribe((res) => {
        this.loading = false;
        if(res.resp){
            //this.userDetail = localStorage.setItem('users', JSON.stringify(res.data));
            alert("User Updated Successfully");
            window.location.reload();
          } else {
            alert(res.Message); 
            let previousSlide =  document.getElementsByClassName("prevslides") as HTMLCollectionOf<HTMLElement>;
             previousSlide[0].click(); 
         }
      })
    } 
  }
  totalMember;
  ngOnInit() {
      
       
        var userDetail = this.userService.CurrrentLoggedinUser();
        this.userDetail = userDetail;
        var userId =  userDetail[0].id;
        this.userService.getUserDetail(userId).subscribe((res) => {


          localStorage.setItem('users', JSON.stringify(res.data));
          var userDetail = this.userService.CurrrentLoggedinUser();
          this.userDetail = userDetail;
          this.totalMember =  parseInt(this.userDetail[0].kids) + parseInt(this.userDetail[0].adults) + parseInt(this.userDetail[0].adults);
       
        })
      $("#default-settings").birthdayPicker();	
      $("#default-birthday").birthdayPicker({"defaultDate":"01-01-1985"});
  }

}
