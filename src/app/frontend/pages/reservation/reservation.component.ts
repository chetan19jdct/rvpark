import { Component, OnInit, ElementRef, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { ReservationService } from '../../services/reservation.service';
import {Router,ActivatedRoute} from '@angular/router';
import { UserService } from '../../services/user.service';
import { Location } from '@angular/common'; 
import { FlatpickrModule } from 'angularx-flatpickr';
declare var $: any;
@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {
  public loading = false;
  showhideothers = false; 
  rentalunit;
  lakefront;
  semi_pullthru;
  backineww;
  standard_pullthru;
  extra_pullthru;
  tentsneww;
  firstScreenVal;
  submitted = false;
  selectedRoom = false;
  percentageValue = 0;
  maximumdats;
  selectedVal = [];
  selectiontents = 'yes';
  startdate;
  endate;
  /* check boxes */
  
  sitesNewArray = [];  
  sitesNewArrayTent = {};  
  /* check boxes end */
  constructor(private elRef:ElementRef,private location: Location, private reserService:ReservationService, private router: Router, private currentUser:UserService,private modalService: NgbModal) {
    this.rentalunit = [{ site_no: '1'},{ site_no: '8'},{ site_no: '10'},{ site_no: '11'},{ site_no: '12' }];
    this.lakefront = [{ site_no: '2'},{ site_no: '3'},{ site_no: '4'},{ site_no: '5'},{ site_no: '6'},{ site_no: '7'}];
    this.semi_pullthru  = [{ site_no: '9'}]; 
    this.backineww  = [{ site_no: '13'},{ site_no: '14'},{ site_no: '15'},{ site_no: '16'},{ site_no: '17'},{ site_no: '18'},{ site_no: '19'},{ site_no: '20'},{ site_no: '21'},{ site_no: '22'},{ site_no: '23'},{ site_no: '24'},{ site_no: '25'},{ site_no: '26'},{ site_no: '27'},{ site_no: '28'},{ site_no: '29'},{ site_no: '30'},{ site_no: '31'},{ site_no: '32'},{ site_no: '33'},{ site_no: '34'},{ site_no: '35'},{ site_no: '36'},{ site_no: '37'},{ site_no: '38'},{ site_no: '39'},{ site_no: '40'},{ site_no: '41'},{ site_no: '42'},{ site_no: '43'},{ site_no: '44'}]; 
    this.standard_pullthru  = [{ site_no: '45'},{ site_no:'46'},{ site_no: '47'},{ site_no: '48'},{ site_no: '49'},{ site_no: '50'},{ site_no:'51'},{ site_no: '52'}];
    this.extra_pullthru  = [{ site_no: '53'},{ site_no: '54'},{ site_no: '55'},{ site_no: '56'},{ site_no: '57'},{ site_no: '58'},{ site_no: '59'},{ site_no: '60'}];
    this.tentsneww  = [{ site_no: 't1'},{ site_no: 't2'},{ site_no: 't3'}];
    this.startdate = new Date();
    //this.endate = new Date();
  }   
  siteMenuToggle = false;
  colorBackground_tab1_sec1;
  colorBackground_tab1_sec2;
 ngOnInit() {
  var rect = { width: 20, height: 10 ,color: 58};
    $('.slideupDown').click(function() {
      if($('#mnss').css('display') == "block") {
        $('#mnss').css('display',"none")
      } else {
        $('#mnss').css('display',"block")
      } 
    })
    /* 
    $('.selectulli > ul > li strong').on('click', function() {
      alert("abcde");
      $('.particularins').hide();
      $(this).siblings('.particularins').slideDown();
   }) 
    */ 
   $("#mnss li").on("click",function(){
    $(this).find(".particularins").slideToggle();
   })

   this.reserService.loadColor().subscribe((res) => {
    this.colorBackground_tab1_sec1 = res[0].color_value;
    this.colorBackground_tab1_sec2 = res[1].color_value;
   
   
   }) 
  }
  
  
  toggle(){
   if(this.siteMenuToggle == false) 
      this.siteMenuToggle = true;
    else 
    this.siteMenuToggle = false; 
  }
  checkboxSelected1(a,b){
    var currentSelectedItem;
   var nuberGet = a;
   if(b){
    $('.putselectval').show();
      $('.putselectval').append('<span id="mynewid'+nuberGet+'"> ' + nuberGet+ ' , </span>');
    } else {
       $('#mynewid'+nuberGet).remove();
    } 
  }
  reservationForm = new FormGroup({
        reserOption : new FormControl(),
        reserOptionss: new FormControl(),
        startdate : new FormControl(),
        endate : new FormControl(),
        rentalunit: new FormControl(),
        lakefront: new FormControl(),
        semi_pullthru: new FormControl(),
        backin: new FormControl(),
        standard_pullthru: new FormControl(),
        extra_pullthru: new FormControl(),
        tents: new FormControl(),
        percentage: new FormControl(),
        reserOption_frst_scr: new FormControl()
  })
 
  tentSelection =  new FormGroup({
    tentSelectionOption : new FormControl(),
  })

  /* ******************* *************** reserOption_frst_scr ************************ ***** */
  reserOption_frst_scr(firstScrnVal) {
      this.firstScreenVal = firstScrnVal.target.value;
      (<HTMLInputElement>document.getElementById("checkdiscountValidation")).value =  this.firstScreenVal;
  }
  /* ******************** ************** reserOption_frst_scr ********************************/
  reserOption(changeVal) {
    var checkedValue = changeVal.target.value;
    if(checkedValue == 'others' && changeVal.target.checked) {
        this.showhideothers = true;
    } else if(checkedValue == 'others' && !changeVal.target.checked){
        this.showhideothers = false;
    }
    /* get percentage  */
      if(changeVal.target.checked) {
        this.selectedVal.push(checkedValue);
      } else {
      var removedIndex = this.selectedVal.indexOf(checkedValue);
        if (removedIndex !== -1) {
          this.selectedVal.splice(removedIndex, 1);
        }  
      }
      if(this.selectedVal.indexOf('pass_america') > -1 || this.selectedVal.indexOf('hapy_camper') > -1){
        this.percentageValue = 50;
      } else if(this.selectedVal.indexOf('Good sam member') > -1){
        this.percentageValue = 30;
      } else if(this.selectedVal.indexOf('Returning Guest member') > -1){
        this.percentageValue = 25;
      }  else {
        this.percentageValue = 20; 
      }
    /* get percentage end  */
  }
  datDefault = false;
  allBookedSites= [];
  datesAlreadyResearved = {};
  maximumdatsNew;
  getdeparturerMin(vals) {
    this.maximumdats = vals.target.value;
    this.maximumdatsNew = vals.target.value;
    var splitted = this.maximumdats.split("-", 3); 
    var newdate  =parseInt(splitted[2]);
    newdate  = newdate+1;
    this.maximumdats = splitted[0] +"-"+ splitted[1]+"-"+newdate; 
     
     $("#endsDat").flatpickr({
       'inline': true,
       'minDate' : this.maximumdats,
       'dateFormat': 'M d, Y',
       
    }); 
     //this.endate =  new Date(this.maximumdats);
     // this.endate = new Date();
     //this.reservationForm.value.endate = this.endate;

    
     this.datDefault = true; 
  }
  departDts(eventss){
     var fromDate =  this.maximumdatsNew
     var departuredt = eventss.target.value;
     this.datesAlreadyResearved = {fromDatekeys: fromDate,toDatekeys:departuredt}
      this.reserService.findAlreadyReser(this.datesAlreadyResearved).subscribe((res) => {
       this.allBookedSites = res.data.bookedSites;
      })
      console.log(this.allBookedSites);
  }
 /* ****************************** all chceck box values end ******************************************/
   
  reservationSubmit(reservationForm){
    localStorage.removeItem('cart');
    var checkLoggedInUser = this.currentUser.CurrrentLoggedinUser();
        this.submitted = true;
        var selectedRoom = document.getElementsByClassName("putselectval") as HTMLCollectionOf<HTMLElement>;
        if(selectedRoom[0].innerHTML == "") {
            alert("Please select atlease one site");
            return;
        } else {  
          if (!this.reservationForm.invalid) {
            this.loading = true;
            const reservationData = this.reservationForm;
           
            this.reservationForm.value.reserOption = JSON.stringify(this.selectedVal); 
            this.reservationForm.value.sitesNewArray = JSON.stringify(this.sitesNewArray); 
            this.reservationForm.value.sitesNewArrayTent = JSON.stringify(this.sitesNewArrayTent); 
            
            this.reservationForm.value.percentage = this.percentageValue;
            this.loading = false;
            var cartData = this.reservationForm;
            var checkCart = this.reserService.reservatoinCart(this.reservationForm.value);
             var bookingCarts = JSON.parse(localStorage.getItem('cart'));
             if(bookingCarts) {
                this.router.navigateByUrl('/BookingCartComponent', {skipLocationChange: true}).then(()=>
                this.router.navigate(["/bookingcart"])); 
              }
          } 
        }
        /* custom code  */
        if(checkLoggedInUser === null) {
          alert("Please do login first");
          this.router.navigateByUrl('/LoginComponent', {skipLocationChange: true}).then(()=>
          this.router.navigate(["/login"])); 
        } 
        /* custom code end */
  } 
  closeResult: string;
  siteID;
  open(content,anc,msg='') {
    var divss = document.querySelector('.putselectval');
    var spanCount = divss.querySelectorAll('span').length;
    if(spanCount < 1) {
        this.siteID = anc;
        if(this.allBookedSites && this.allBookedSites.indexOf(this.siteID) >= 0) {
            alert('Site is already booked');
        } else {
            const modalRef = this.modalService.open(content);
           
        } 
      } else {
        alert('You can select maximum one site');
      } 
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  tempsite = '';
  tentSelections(tentSelection,siteId) {
    this.tempsite = siteId;
    var tentOption =  this.tentSelection.value.tentSelectionOption;
    this.siteCheck(siteId,tentOption);
    this.modalService.dismissAll('as');
  }
  siteCheck(siteNo,tentOption) {
    var siteChecked = <HTMLInputElement>document.getElementById("site_"+siteNo); 
    if(siteChecked) {
      if(siteChecked.checked  == true) { 
        siteChecked.checked = false; 
        var removedIndex = this.sitesNewArray.indexOf(siteNo);
        if (removedIndex !== -1) {
          this.sitesNewArray.splice(removedIndex, 1);
        }   
      } else {
        siteChecked.checked = true; 
        this.sitesNewArray.push(siteNo);
        this.sitesNewArrayTent[siteNo] = tentOption;
      }
      this.checkboxSelected1(siteNo,siteChecked.checked);
    }
    console.log(this.sitesNewArrayTent);
  }
  cleanSite() {
    var divss = document.querySelector('.putselectval');
    var spanCount = divss.querySelectorAll('span').length;
    var siteNo = this.tempsite;
    var siteChecked = <HTMLInputElement>document.getElementById("site_"+siteNo); 
    if(siteChecked) {
      if(siteChecked.checked  == true) { 
        siteChecked.checked = false; 
        var removedIndex = this.sitesNewArray.indexOf(siteNo);
        if (removedIndex !== -1) {
          this.sitesNewArray.splice(removedIndex, 1);
        }   
      } 
     
    }
    this.checkboxSelected1(siteNo,siteChecked.checked);



  }
}
