import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from "../../services/user.service";

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  constructor(private userservice: UserService) {}
  ngOnInit() {
    
  }
  submitted;
  emailaddress = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'; 
  contactusform = new FormGroup({
    fname : new FormControl(''),
    lname : new FormControl(''),
    emailcontact : new FormControl('',[Validators.pattern(this.emailaddress)]),
    textmessage : new FormControl(''), 
  })
  contactusforms(contactusform) {
    this.submitted = true;
    if(!this.contactusform.invalid) {
        var formValues = this.contactusform.value;
        this.userservice.contactSubmit(formValues).subscribe((res) => {
          alert(res.Message);
        })
    }
  }
}
