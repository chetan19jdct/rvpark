import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AurorahistoryComponent } from './aurorahistory.component';

describe('AurorahistoryComponent', () => {
  let component: AurorahistoryComponent;
  let fixture: ComponentFixture<AurorahistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AurorahistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AurorahistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
