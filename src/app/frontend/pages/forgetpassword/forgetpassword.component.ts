import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {

  constructor(private currentUser:UserService, private router: Router) { }
  ngOnInit() {}
  submitted;
  emailaddress = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'; 
  forgetPassForm = new FormGroup({
    loginemail : new FormControl('', [Validators.email]),
   })
  forgetPass(forgetPassForms) {
    this.submitted = true;
    var email = this.forgetPassForm.value;
    if(this.forgetPassForm.valid) {
      this.currentUser.forgetPass(email).subscribe((res)=>{
        alert("Please Check email. Reset link is sent to your email address");
        this.router.navigate(['/#']);
      });
    }
  }
}
