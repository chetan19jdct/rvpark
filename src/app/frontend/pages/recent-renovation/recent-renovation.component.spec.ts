import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentRenovationComponent } from './recent-renovation.component';

describe('RecentRenovationComponent', () => {
  let component: RecentRenovationComponent;
  let fixture: ComponentFixture<RecentRenovationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentRenovationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentRenovationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
