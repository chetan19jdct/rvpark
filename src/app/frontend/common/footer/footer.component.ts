import { Component, OnInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() {}
  ngOnInit() {
    $('#testimonialssl').owlCarousel({
      loop: true,
      margin: 0,
      animateIn: 'fadeIn',
      animateOut: 'fadeOut',
      slideSpeed: 15000,
      autoplay: true,
      autoplayTimeout: 6000,
      autoplayHoverPause: true,
      autoHeight: true,
      nav: true,
      responsive: {
        0: {
          items: 1
        },
        600: {
          items: 1
        },
        1000: {
          items: 1
        }
      }
    })
  }
}
