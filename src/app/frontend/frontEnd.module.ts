import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { FormsModule, ReactiveFormsModule , Validators} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { FrontendComponent } from './frontEnd.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { RegisterComponent } from './pages/register/register.component';
import { DashboardService } from './services/dashboard.service';
import { LoginComponent } from './pages/login/login.component';
import { UserprofileComponent } from './pages/userprofile/userprofile.component';
import { UserService } from './services/user.service';
import { FlatpickrModule } from 'angularx-flatpickr';
import { ReservationComponent } from './pages/reservation/reservation.component';
import { NgxLoadingModule } from 'ngx-loading';
import { DashboardModule } from '../dashboard/dashboard.module';
import { AuthGuard } from '../guards/auth.guard';
import { BookingCartComponent } from './pages/booking-cart/booking-cart.component';

import { NgxPayPalModule } from 'ngx-paypal';
import { UserOrderComponent } from './pages/user-order/user-order.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ContactusComponent } from './pages/contactus/contactus.component';
import { ForgetpasswordComponent } from './pages/forgetpassword/forgetpassword.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { AurorahistoryComponent } from './pages/aurorahistory/aurorahistory.component';
import { RecentRenovationComponent } from './pages/recent-renovation/recent-renovation.component';
import { FuturePlanComponent } from './pages/future-plan/future-plan.component';

const appRoutess: Routes = [
  { 
    path: '', component: FrontendComponent,
    children: [
      { path: '', component: HomeComponent},
      { path: 'registration', component: RegisterComponent , canActivate: [AuthGuard],data: {roles: 'user'}},
      { path: 'login', component: LoginComponent, canActivate: [AuthGuard],data: {roles: 'user'}},
      { path: 'profile', component: UserprofileComponent, canActivate: [AuthGuard],data: {roles: 'user_admin'}},
      { path: 'reservation', component: ReservationComponent},
      { path: 'bookingcart', component: BookingCartComponent},
      { path: 'ordersuser', component: UserOrderComponent},
      { path: 'forgetpassword', component: ForgetpasswordComponent},
      { path: 'resetPassword/:key', component: ResetPasswordComponent},
      { path: 'contact', component: ContactusComponent, canActivate: [AuthGuard],data: {showPopup: 'showPopup'}},
      { path: 'history', component: AurorahistoryComponent},
      { path: 'renovations', component: RecentRenovationComponent},
      { path: 'futureplan', component: FuturePlanComponent},
	    { path: '**', redirectTo: '/' } 
    ],
  },
]; 
export const adminRouting = RouterModule.forChild(appRoutess)
@NgModule({
  declarations: [
    FrontendComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    UserprofileComponent,
    ReservationComponent,
    BookingCartComponent,
    UserOrderComponent,
    ContactusComponent,
    ForgetpasswordComponent,
    ResetPasswordComponent,
    AurorahistoryComponent,
    RecentRenovationComponent,
    FuturePlanComponent  
  ],exports:[
    RegisterComponent,
   ],
  imports: [
    BrowserModule,
    NgxPayPalModule,
    NgbModule,
    HttpModule,
    HttpClientModule,
    NgxLoadingModule.forRoot({}),
    ReactiveFormsModule,
    ReactiveFormsModule,
    FormsModule, 
    DashboardModule,
    FlatpickrModule.forRoot(),
	  RouterModule.forRoot(
      appRoutess,
      { enableTracing: true } // <-- debugging purposes only
    ),  
  ],
  providers: [DashboardService,UserService,AuthGuard],
  bootstrap: [FrontendComponent]
})
export class FrontendModule { }
