import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule , Validators} from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgxLoadingModule } from 'ngx-loading';
import { DashboardComponent } from './dashboard.component';
import { FlatpickrModule } from 'angularx-flatpickr';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { DashboardHomeComponent } from './pages/dashboard-home/dashboard-home.component';
import { UsersComponent } from './pages/users/users.component';
import { DashboardService } from './services/dashboard.service';
import { ReservationListComponent } from './pages/reservation-list/reservation-list.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { AdminLoginComponent } from './pages/admin-login/admin-login.component';
import { AuthGuard } from '../guards/auth.guard';
import { ColorOptionComponent } from './pages/color-option/color-option.component';
import { ColorPickerModule } from 'ngx-color-picker';

const appRoutess: Routes = [
  {
    path: 'dashboard', component: DashboardComponent,
    children: [
      { path: '', component: DashboardHomeComponent,canActivate: [AuthGuard],data: {roles: 'admin'} },
      { path: 'admin', component: AdminLoginComponent,data: {roles: 'admin_loginPage'}}, 
      { path: 'user', component: UsersComponent, canActivate: [AuthGuard],data: {roles: 'admin'}},
      { path: 'colors', component: ColorOptionComponent, canActivate: [AuthGuard],data: {roles: 'admin'}},
      { path: 'reservations', component: ReservationListComponent, canActivate: [AuthGuard],data: {roles: 'admin'}},
      { path: 'profile/:userId', component: UserProfileComponent, canActivate: [AuthGuard],data: {roles: 'admin'}},
     ],
  } 
]; 
export const adminRouting = RouterModule.forChild(appRoutess)
@NgModule({
  declarations: [
   HeaderComponent,
   FooterComponent,
   DashboardComponent,
   DashboardHomeComponent,
   UsersComponent,
   ReservationListComponent,
   UserProfileComponent,
   AdminLoginComponent,
   ColorOptionComponent
  ],exports:[
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    ColorPickerModule,
    NgxLoadingModule.forRoot({}),
    ReactiveFormsModule,
    ReactiveFormsModule,
    FormsModule, FlatpickrModule.forRoot(),
	  RouterModule.forRoot(
      appRoutess,
      { enableTracing: true } // <-- debugging purposes only
    ),  
  ],
  providers: [DashboardService,AuthGuard],
  bootstrap: [DashboardComponent]
})
export class DashboardModule { }
