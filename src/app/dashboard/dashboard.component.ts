import { Component } from '@angular/core';
import { UserService } from '../frontend/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  title = 'rvpark';
  constructor(private userService:UserService, private router: Router) { }
  ngOnInit() {
    
    (function () {
      "use strict";
      var treeviewMenu = $('.app-menu');
          $('[data-toggle="sidebar"]').click(function(event) {
          event.preventDefault();
          $('.app').toggleClass('sidenav-toggled');
      });
      $("[data-toggle='treeview']").click(function(event) {
        event.preventDefault();
        if(!$(this).parent().hasClass('is-expanded')) {
          treeviewMenu.find("[data-toggle='treeview']").parent().removeClass('is-expanded');
        }
         $(this).parent().toggleClass('is-expanded');
        });
        $("[data-toggle='treeview.'].is-expanded").parent().toggleClass('is-expanded');
        $("[data-toggle='tooltip']").tooltip();
    
    })();
    /* check used logged in  */
    /*var userDetail = this.userService.CurrrentLoggedinUser();
     if(userDetail) {
       if(userDetail[0].role == "user") {
         this.router.navigate(['/'])
        } else {
         this.router.navigate(['/dashboard'])
        }
     } else {
      this.router.navigate(['/dashboard/admin'])
     }*/
    /*  checked user logged in  */
    
  }
}
