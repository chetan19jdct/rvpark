import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../frontend/services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private currentUser: UserService, private router: Router) { }
  ngOnInit() {}
  logout1(){
    this.currentUser.loggedOutUser();
    this.router.navigateByUrl('/AdminLoginComponent', {skipLocationChange: true}).then(()=>
    this.router.navigate(["/dashboard/admin"])); 
  }

}
