import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControlName, FormControl } from '@angular/forms';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-color-option',
  templateUrl: './color-option.component.html',
  styleUrls: ['./color-option.component.css']
})
export class ColorOptionComponent implements OnInit {

  constructor(private dash: DashboardService) { }

  ngOnInit() {}
  colorform =  new FormGroup({
    color1_1 : new FormControl(),
    color1_2 : new FormControl(),
  })
  submitted = false;
  public loading = false;
  colorSubmit(colorform) {
    var colorVal = this.colorform.value;
    this.submitted = true;
    if(this.colorform.value.color1_1  && this.colorform.value.color1_2){
      this.dash.colorSubmit(colorVal).subscribe((res) => {
        if(res.resp == '1') {
          alert(res.Message);
        } else {
          alert(res.Message);
        }
      });
    } else {
      alert('Please insert both fields');
    }
  }
}
