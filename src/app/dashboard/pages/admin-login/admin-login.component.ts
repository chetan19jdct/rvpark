import { Component, OnInit , ViewChild, ElementRef} from '@angular/core';
import { UserService } from '../../../frontend/services/user.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DashboardService } from '../../../frontend/services/dashboard.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  constructor(private userSer: UserService,private dashboardservice: DashboardService, private router: Router) { }
  loginForm = new FormGroup({
    loginemail : new FormControl('', [Validators.email]),
    password : new FormControl(),
  })
  submitted = false;
  public loading = false;
  ngOnInit() {
     var userDetail = this.userSer.CurrrentLoggedinUser();
    if(this.userSer.CurrrentLoggedinUser()) {
            if(userDetail[0].role == "user") {
                this.router.navigate(['/#']);
                return false;
            } else {
                this.router.navigate(['/dashboard']);
                return false;
            }
        } 
    }
  get f() { return this.loginForm.controls; }
  loginSubmit(loginForm) {
    this.submitted = true;
    var loginFormValue = this.loginForm.value;
    if (!this.loginForm.invalid) {
      this.loading = true; 
      this.dashboardservice.checkLogin(loginFormValue).subscribe((res) => {
          this.loading = false;
          if((res.resp)) {
            localStorage.setItem('users', JSON.stringify(res.data));
            if(res.data[0].role == "user") {
                this.router.navigate(['/#']);
              } else {
                this.router.navigate(['/dashboard'])
              }  
           } else {
            alert("Invalid Credential" );
          }
      });
    } 
  }
}
