import { Component, OnInit , ViewChild, ElementRef} from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';


@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.css']
})
export class DashboardHomeComponent implements OnInit {
  
  constructor(private dashSer: DashboardService) { }
  ngOnInit() {
    this.userList();  
  }
  dataPara = {};
  allUserList = [];
  public loading = false;
  userList(){
      this.loading = true;
      this.dashSer.userlist(this.dataPara).subscribe((res) => {
         if(res.resp) {
          this.loading  = false;
          this.allUserList = res.data;
         }
      })
  }

}
