import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-reservation-list',
  templateUrl: './reservation-list.component.html',
  styleUrls: ['./reservation-list.component.css']
})
export class ReservationListComponent implements OnInit {
  public loading = false;
  constructor(private dashSer: DashboardService) { }
  ngOnInit() {
    this.resrvations();
  }
  reservationList = [];
  resrvations(){
   this.loading = true;
   this.dashSer.reservationlist().subscribe((res) => {
    
       if(res.resp) {
          this.loading = false;
          this.loading = false;
          this.reservationList = res.data;
          for(var x = 0; x <  this.reservationList.length ; x ++) {
            if(this.reservationList[x].status.length > 10) {
              /// payment status completed or pending 
              //console.log(this.reservationList[x]);
               this.reservationList[x].status = JSON.parse(this.reservationList[x].status); 
               console.log(this.reservationList[x].status);
               this.reservationList[x].statusssssss =  this.reservationList[x].status.status; 
               this.reservationList[x].paymentModess = this.reservationList[x].status.paymentMode;
              //this.reservationList[x].status = this.reservationList[x].status.status;

              /**/
            } else {
              this.reservationList[x].statusssssss = 'Completed'; 
              this.reservationList[x].paymentModess = 'Online'; 
            }
          } 
        } else {
          this.loading = false;
        }
    })
  }
}
