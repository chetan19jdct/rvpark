import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DashboardService } from '../../services/dashboard.service';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  userId;
  userDetail;
  public loading = false;
  constructor(private activeRoute: ActivatedRoute, private dashboardSer: DashboardService ) {}
  ngOnInit() {
    this.loading = true;
    const routeParams = this.activeRoute.snapshot.params;
    this.userId = routeParams.userId;
    this.dashboardSer.userDeatail(this.userId).subscribe((res) => {
      if(res.resp) {
        this.loading = false;
        this.userDetail = res.data[0];
        console.log(this.userDetail);
      } else {
        alert("Invalid Action");
      }
    })
  }
}
