import { Injectable } from '@angular/core';
import { HttpHeaders, HttpParams, HttpClient } from '@angular/common/http';
import { map, filter, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private base_url = environment.engineUrl;
  constructor(private http: HttpClient) { 
}
  headerCommon() {
    return {
      headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
    }; 
  }
  userlist(reservationFormValues):Observable<any>{
    let params = new HttpParams({fromObject:reservationFormValues});
    return this.http.get(this.base_url+'user/allUser', this.headerCommon()).pipe(map((response: Response) => {
      return response;
    }
  ));
  }
  reservationlist():Observable<any>{
    return this.http.get(this.base_url+'user/reservationList', this.headerCommon()).pipe(map((response: Response) => {
      return response;
    }
  ));
  }
  userDeatail(userID) : Observable<any> {
    let params = new HttpParams().set('userID',userID);
    return this.http.post(this.base_url+'user/userDetail', params, this.headerCommon()).pipe(map((response: Response) => {
      return response;
    }
    ));
  }
  colorSubmit(colorSubmit) : Observable<any> {
    let params = new HttpParams({fromObject:colorSubmit});
    return this.http.post(this.base_url+'user/colorSubmit', params, this.headerCommon()).pipe(map((response: Response) => {
      return response;
    }
    ));

  }
  /*userDeatail(userID):Observable<any> {
    let params = new HttpParams().set('userID',userID);
    return this.http.post(this.base_url+'user/userDetail', params, this.headerCommon()).pipe(map((response: Response) => {
      return response;
    }
    ));
  } */


 
}
