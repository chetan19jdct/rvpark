import { Injectable } from '@angular/core';
import { CanActivate,ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { UserService } from '../frontend/services/user.service';
declare var $: any;
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) {} 
  canActivate(
    routesss: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      var userDetail = this.userService.CurrrentLoggedinUser();
      if(routesss.data.showPopup) {

        $(".my_img").show();
        $('.testimonials').show();
        $('.appendcontent, .img-appends').hide();
      }
      if(routesss.data.roles == "admin") {
            if(this.userService.CurrrentLoggedinUser()) {
              if(userDetail[0].role == "user") {
                this.router.navigate(['/']);
                return false;
              } else {
                return true;
              }
          } else {
              this.router.navigate(['/dashboard/admin']);
              return false;
          }
        } else if(routesss.data.roles == "admin_loginPage") {
            if(this.userService.CurrrentLoggedinUser()){
              if(userDetail[0].role == "user") {
                this.router.navigate(['/']);
                return false;
              } else {
                this.router.navigate(['/dashboard']);
                return false;
              }
            } else {
              return true;
            }
        } else if(routesss.data.roles == "user") {
              if(this.userService.CurrrentLoggedinUser()) {
                this.router.navigate(['/']);
                return false;
              } else { 
                return true;
              }
        } else if(routesss.data.roles == "user_admin") {
          if(!this.userService.CurrrentLoggedinUser()) {
            this.router.navigate(['/']);
            return false;
          } else { 
            return true;
          }
        } else {
          return true;
        }
      
  }
 
}
